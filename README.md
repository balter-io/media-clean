# Media Clean

This script takes a root folder location as a command line argument and looks for files with extensions matching 
those in the list within the code. It then deletes them. These files are generally information files or nuisance files
that are unnecessary when saving the torrent file.
