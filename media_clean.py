#!/usr/bin/env python

import os
import sys


def main(path):
    extensions = ('.txt', '.jpg', '.jpeg', '.nfo', '.png', '.bmp')

    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith(extensions):
                os.unlink(os.path.join(root, file))
                print('Removing File:', os.path.join(root, file))

    for directories, names, files in os.walk(path, topdown=False):
        if not names:
            os.rmdir(directories)
            print('Removing Directory:', directories)


if __name__ == '__main__':
    filthy = sys.argv[1]
    main(filthy)
